# Add a splash screen to a React Native app (2021 edition)
> React Native project

This is the final code for the [Add a splash screen to a React Native app in 2021](https://dev.to/stack-labs/add-a-splash-screen-to-a-react-native-app-in-2021-e6n) tutorial.

## Setup
Checkout the project

Install dependencies
```
yarn install
cd ios; pod install; cd ..
```

Run the project
```
yarn ios
```
or
```
yarn android
```
